# eukim <eukim@redhat.com>, 2013. #zanata
# KimJeongYeon <jeongyeon.kim@samsung.com>, 2017.
# Sangchul Lee <sc11.lee@samsung.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: pipewire\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pipewire/pipewire-media-"
"session/issues/new\n"
"POT-Creation-Date: 2021-10-20 10:03+1000\n"
"PO-Revision-Date: 2018-06-21 15:10+0900\n"
"Last-Translator: Sangchul Lee <sc11.lee@samsung.com>\n"
"Language-Team: Korean\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: src/alsa-monitor.c:662
msgid "Built-in Audio"
msgstr "내장 오디오 "

#: src/alsa-monitor.c:666
msgid "Modem"
msgstr "모뎀 "

#: src/alsa-monitor.c:675
msgid "Unknown device"
msgstr ""
